<?php

namespace App\packages\report\src\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Exports\Export;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
class ExportController extends Controller
{
    public function export(Request $request)
    {
        if (session()->has('page_title')) {
            // return Excel::download(new Export(), 'users.csv');
            $file_name = session()->get('page_title').".xlsx";
            return Excel::download(new Export(), $file_name);
        }
    }
}
