<?php

namespace App\packages\report\src\Controllers;

use App\common\Common;
use App\Exports\Export;
use App\packages\report\src\models\MasterModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Array_;
use App\Http\Controllers\Controller;

class ReportSettingController extends Controller
{

    public function index(Request $request)
    {
       // dd($request);
        $requestData        = $request->all();
        $sessionData        = $request->session()->get('user_info');
        $masterModel        = new MasterModel();

        try {
            $addEditFormKey = 'report';

            $settings = [
                'action_edit_button'        => '1',
                'action_view_button'        => '1',
                'action_accordion_button'   => '0',
                'accordion_url'             => '',
                'add_button'                => '1',
                'pagination'                => '1',
                'filter_button'             => '0',
                'group_button'              => '1',
                'export_button'             => '1',
                'import_button'             => '0',
                'search'                    => '1',
                'list_title'                => 'Report Setting',
                'action_header'             => '1',
                'multiSaveKey'              => '',
            ];
            $headers = [
                'id'                        => 'Sr. No',
                'report_name'               => 'Report Name',
                'route_name'                => 'Route'
            ];

            $query = DB::table('report');

            if(isset($requestData['basics']) && !empty($requestData['basics'])) {
                $query->where(function ($query) use ($requestData) {
                    $query->where('report_name', 'LIKE', '%' . $requestData['basics'].'%');
                });
            }

            if(isset($requestData['filter_column_name']) && isset($requestData['sorting_method']) && !empty($requestData['filter_column_name']) && !empty($requestData['sorting_method'])){
                $query      = $query->orderBy($requestData['filter_column_name'], $requestData['sorting_method']);
            } else {
                $query      = $query->orderBy('id', 'DESC');
            }

            $export     = new Export();
            $sql_query  = $export->eloquentSqlWithBindings($query);
            $export->setExcelParameters($sql_query, $headers, $settings['list_title']);

            $paginationData = $query->paginate(10);
            $listingData = json_decode(json_encode($paginationData), true);

        }catch(\Illuminate\Database\QueryException $ex){
            //  $common     = new Common();
            //  $common->error_logging($ex, 'index', 'ReportSettingController.php');
            //  return view('errors.oh!');
            dd($ex->getMessage());
            dd($ex->getLine());
        }

        $filterFormData = '';
        $filterFields = '';

        return view('report::layouts.listing-container',compact('headers','listingData', 'paginationData', 'settings', 'addEditFormKey', 'filterFormData', 'filterFields'));
    }

    /*
     @ Show Form Field listing view.
     */
    public function show(Request $request){
        $requestData = $request->all();
        try {
            $report_id              = $requestData['id'];
            $reportData             = DB::table('report')->where('id', $report_id)->first();
            $reportAttributeData    = DB::table('report_attribute')->where('report_id', $report_id)->orderBy('id', 'DESC')->paginate(10);

        }catch(\Illuminate\Database\QueryException $ex){
            // $common     = new Common();
            // $common->error_logging($ex, 'show', 'FormSettingController.php');
            // return view('errors.oh!');
            dd($ex->getMessage());
            dd($ex->getLine());
        }
        return view('report::super_admin.Master.report_setting.reportSetting', compact('reportData','report_id', 'reportAttributeData'));
    }

    /*
    @ Store form field.
    */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $masterModel = new MasterModel();

        try {
            $insertField['report_id']           = $requestData['report_id'];
            $insertField['attribute_name']      = $requestData['attribute_name'];

            $masterModel->insertData($insertField,'report_attribute');

        }catch(\Illuminate\Database\QueryException $ex){
            // $common     = new Common();
            // $common->error_logging($ex, 'store', 'ReportSettingController.php');
            // return view('errors.oh!');
            dd($ex->getMessage());
            dd($ex->getLine());
        }
        return redirect('report_setting/edit?id='.$requestData['report_id'])->with('success','Field Created successfully');
    }

    /*
     @ Edit Form Field.
     */
    public function editAttributeField(Request $request)
    {
        $requestData = $request->all();
        $masterModel = new MasterModel();

        try {
            $where['id'] = $requestData['attribute_id'];
            $fieldDetails = $masterModel->getMasterRow('report_attribute', $where);
            return json_encode($fieldDetails);

        }catch(\Illuminate\Database\QueryException $ex){
            // $common     = new Common();
            // $common->error_logging($ex, 'Edit', 'ReportSettingController.php');
            // return view('errors.oh!');
            dd($ex->getMessage());
            dd($ex->getLine());
        }
    }

    /*
    @ Store form field.
    */
    public function updateReportAttibutr(Request $request)
    {
        $requestData = $request->all();
       // dd($requestData);
        $masterModel = new MasterModel();

        try {
            $updateField['attribute_name']            = $requestData['edit_attribute_name'];
            //dd($updateField);
            $where['id']                    = $requestData['attribute_id'];
            $masterModel->updateData($updateField, 'report_attribute', $where);

        }catch(\Illuminate\Database\QueryException $ex){
            // $common     = new Common();
            // $common->error_logging($ex, 'Update', 'ReportSettingController.php');
            // return view('errors.oh!');
            dd($ex->getMessage());
            dd($ex->getLine());
        }
        return redirect('report_setting/edit?id='.$requestData['report_id'])->with('success','Field Updated successfully');
    }
}
