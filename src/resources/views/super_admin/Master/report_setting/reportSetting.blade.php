@extends('report::layouts.app')

@section('content')
<div class="container main-body">
    <div class="row">
        <div class="col-md-12">
            <?php if (Session::has('success')) : ?>
                <div class="alert alert-success">
                    <?php echo (Session::get('success')); ?>

                </div>
            <?php endif; ?>
            <a href={{ url('report_setting') }}>Back</a>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Report Attribute</a>
                </li>
            </ul>
            <div class="tab-content pt-1" id="myTabContent">
                <!-- Form Fields -->
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn-sm btn-sky-blue float-right" data-toggle="modal" data-target="#modalPopUpSetting"> Add Fields </button>
                    <span class="h6 font-weight500 text-orange"> Details of {{ $reportData->report_name }}</span>
                    <!-- Modal -->

                    <div class="table-responsive">
                        <table class="table table-border text-center mt-1">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="general_mst_data">
                                @foreach($reportAttributeData as $attribute)
                                    <tr>
                                        <td> {{ $attribute->id }}</td>
                                        <td> {{ $attribute->attribute_name }}</td>
                                        <td class="padding7px">
                                            <button type="button" class="btn btn-sm table-row-btn btnEditFields" id="editFormFields" data-toggle="modal" data-target="#formFieldEdit" data-attributeId="{{ $attribute->id }}">
                                                <span class="fa fa-edit"></span>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="no_of_record">

                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div id="pagination">

                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<!-- Add Form Fields -->
<div class="modal fade" id="modalPopUpSetting" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Report Fields
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </h5>
            </div>
            <div class="modal-body">
                <form id="" role="form" method="post" action="{{ route('report_setting.store') }}">
                    @csrf
                    <input type="hidden" value="{{ $report_id }}" name="report_id">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txtLabelName">Attribute Name</label>
                                <input type="text" name="attribute_name" class="form-control border-sky-blue" id="attribute_name" required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <button type="reset" class="btn-sm btn-outline-sky-blue fa-pull-right">Reset</button>
                            <button type="submit" class="btn-sm btn-sky-blue fa-pull-right mr-1">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Edit Form Fields -->
<div class="modal fade" id="formFieldEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Report Attribute<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </h5>
            </div>
            <div class="modal-body">
                <form id="" role="form" method="post" action="update">
                    @csrf
                    <input type="hidden" value="{{ $report_id }}" name="report_id">
                    <input type="hidden" id="old_attribute_id" value="" name="attribute_id">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txtLabelName">Attribute Name</label>
                                <input type="text" name="edit_attribute_name" class="form-control border-sky-blue" id="edit_attribute_name" required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <button type="reset" class="btn-sm btn-outline-sky-blue fa-pull-right">Reset</button>
                            <button type="submit" class="btn-sm btn-sky-blue fa-pull-right mr-1">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>

    $('.btnEditFields').on('click', function() {
        var attribute_id = $(this).attr("data-attributeId");

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: APP_URL + '/editAttribute',
            data: {
                'attribute_id': attribute_id
            },
            success: function(ret_data) {

                var data = JSON.parse(ret_data);
                console.log(data);
                $('#old_attribute_id').val(attribute_id);
                $('#edit_attribute_name').val(data.attribute_name);
            }
        });
    });
</script>
@endsection
