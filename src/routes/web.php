<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['namespace' => 'App\packages\report\src\controllers', /*'prefix' => 'report',*/ 'middleware' => ['web']], function() {

/*-- Report Settings --*/

Route::resource('report_setting', 'ReportSettingController');
Route::get('report/edit', 'ReportSettingController@show');
Route::post('editAttribute', 'ReportSettingController@editAttributeField');
Route::post('report_setting/update', 'ReportSettingController@updateReportAttibutr');

Route::post('report/update', 'ReportSettingController@updateReportAttibutr');
Route::get('generate_form', 'FormsController@index');
Route::post('generic_save', 'FormsController@genericSave');

/*------------ Export Excel --------------*/

Route::get('export_to_excel', 'ExportController@export');

});


